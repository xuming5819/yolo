import torch
import torch.nn as nn

a = torch.cuda.is_available()

print(a)

import copy

a = [1, 2, [1, 2]]
print(a)

b = a
print(a)
c = copy.copy(a)
print(c)
d = a[:]
print(d)
e = copy.deepcopy(a)
print(e)
a.append(3)
a[2].append(3)
print(a)


print(b)

