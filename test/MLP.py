from torch import nn
import torch
from torch import optim


class MLP(nn.Module):
    def __init__(self, in_dim, hid_dim1, hid_dim2, out_dim):
        super(MLP, self).__init__()
        # 通过 Sequential 快速搭建三层感知机层
        self.layer = nn.Sequential(
            nn.Linear(in_dim, hid_dim1),
            nn.ReLU(),
            nn.Linear(hid_dim1, hid_dim2),
            nn.ReLU(),
            nn.Linear(hid_dim2, out_dim),
            nn.ReLU()
        )

    def forward(self, x):
        x = self.layer(x)
        return x


net = MLP(28*28, 300, 200, 1)
print(net)

optimizer = optim.SGD(params=net.parameters(), lr=0.01)
print(optimizer)

input = torch.randn(1, 28*28)
print(input, input.shape)

output = net(input)
print(output)

label = torch.Tensor([1, 0, 4, 7, 9, 3, 4, 5, 3, 2]).long()
print(label)

criterion = nn.CrossEntropyLoss()
loss = criterion(output, label)
print(loss)

optimizer.zero_grad() # 清空梯度