import torch
import torch.nn as nn


class Perception(nn.Module):
    def __init__(self, in_dim, hid_dim, out_dim):
        super(Perception, self).__init__()
        # 利用 nn.Sequention() 快速搭建网络模块
        self.layer = nn.Sequential(
            nn.Linear(in_dim, hid_dim),
            nn.Sigmoid(),
            nn.Linear(hid_dim, out_dim),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.layer(x)
        return y


net = Perception(100, 1000, 10).cuda()

print(net)

input = torch.randn(100).cuda()
print(input.shape)

output = net(input)

print(output)


