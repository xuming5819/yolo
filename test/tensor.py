import torch

# a = torch.Tensor(2, 2) # a 没有指定类型
# print(a)
#
# b = a.double()  # b 的类型变为
# print(b)
#
# c = a.type(torch.DoubleTensor) # a 的类型变为
# print(c)
#
# d = a.type_as(b)  # 将 a 的类型变为 b 的类型
# print(d)
#
#
# ## ---------------------------------------

# Tensor 的创建与维度查看

a = torch.Tensor(2, 2)
print(a)

b = torch.DoubleTensor(2, 2)
print(b)

c = torch.Tensor([[1, 2], [3, 4]])
print(c)

d =  torch.zeros(2, 2)
print(d)

e = torch.ones(2, 2)
print(e)


f = torch.eye(3, 3)
print(f)

g = torch.randn(2, 2)
print(g)

h = torch.arange(1, 6, 3)
print(h)

j = torch.linspace(1, 6, 2)
print(j)

k = torch.tensor([1, 2, 3])
print(k)

# --------------------------------------
# 查看维度和元素个数

a = torch.randn(2, 10)

print(a, a.shape)

print(a.size())

print(a.numel())  # 查看Tensor 中元素的个数
print(a.nelement())
# ---------------------------------------


a = torch.Tensor([[1, 2],[3, 4]])
print(a)

b = torch.Tensor([[5, 6], [7, 8]])
print(b)

# 以第一维度进行拼接
c = torch.cat([a, b], 0)
print(c)
print(c.shape)
d = torch.cat([a, b], 1)
print(d)

# --------------
# torch.stack() 函数是用来新增维度

e = torch.stack([a, b], 0)
print(e)
print(e.shape)

f = torch.stack([a, b], 1)
print(f)
print(f.shape)

g = torch.stack([a, b], 2)
print(g)